from typing import Optional

from src.common.exceptions.http import BaseHTTPException


class CreationHTTPException(BaseHTTPException):

    status: int = 422

    def __init__(self, table_name: str, data: dict):
        self.message = (
            f"Object in table {table_name} with field values {data} " f"had been already created"
        )


class NoFieldHTTPException(BaseHTTPException):
    status: int = 400

    def __init__(self, table_name: str, field: str):
        self.message = f"In table {table_name} no such field {field}"


class NotFoundHTTPException(BaseHTTPException):
    status: int = 404
    message: str = "Object does not exist"

    def __init__(self, message: Optional[str] = None) -> None:
        if message:
            self.message = message
