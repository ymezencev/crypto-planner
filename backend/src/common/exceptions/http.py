from src.common.exceptions import BaseAppException


class BaseHTTPException(BaseAppException):
    """Базовое исключение запроса http."""

    status: int
    message: str
