class BaseAppException(Exception):
    """Базоовое исключение приложения."""

    message: str
