from src.common.exceptions import BaseAppException


class BaseRepoException(BaseAppException):
    """Базоовое исключение репозитория."""


class ObjectExistsException(BaseRepoException):
    """Объект уже существует."""


class NoFieldException(BaseRepoException):
    """Отсутствует поле."""

    def __init__(self, field: str):
        self.field = field


class NotFoundException(BaseRepoException):
    """Объект не найден."""
