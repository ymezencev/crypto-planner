from sqlalchemy.ext.asyncio import create_async_engine

from config.settings import settings


engine = create_async_engine(url=settings.database.dsn, echo=True, pool_pre_ping=True, future=True)
