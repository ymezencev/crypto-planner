import abc
from typing import Generic, TypeVar, Union
from uuid import UUID

from src.common.domain.dto import BaseInSchema, BaseOutSchema


SchemaOutType = TypeVar("SchemaOutType", bound=BaseOutSchema)
SchemaInType = TypeVar("SchemaInType", bound=BaseInSchema)


class IBaseRepository(abc.ABC, Generic[SchemaInType, SchemaOutType]):
    @abc.abstractmethod
    async def create(self, new_object: SchemaInType) -> SchemaOutType:
        ...

    @abc.abstractmethod
    async def get_by_id(self, object_id: Union[int, UUID]) -> SchemaOutType:
        ...

    @abc.abstractmethod
    async def get_all(self) -> list[SchemaOutType]:
        ...

    @abc.abstractmethod
    async def update(
        self, object_id: Union[int, UUID], updated_data: SchemaInType
    ) -> SchemaOutType:
        ...

    @abc.abstractmethod
    async def delete(self, object_id: Union[int, UUID]) -> None:
        ...
