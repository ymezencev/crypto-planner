from typing import Generic, Type, TypeVar, Union
from uuid import UUID

from fastapi_async_sqlalchemy import db
from pydantic import parse_obj_as
from sqlalchemy import delete, exc, select, update
from sqlalchemy.engine import CursorResult

from src.common.data.models import Base
from src.common.data.repositories import IBaseRepository
from src.common.domain.dto import BaseInSchema, BaseOutSchema
from src.common.exceptions.repository import NotFoundException


ModelType = TypeVar("ModelType", bound=Base)
SchemaOutType = TypeVar("SchemaOutType", bound=BaseOutSchema)
SchemaInType = TypeVar("SchemaInType", bound=BaseInSchema)


class BaseRepository(
    Generic[ModelType, SchemaInType, SchemaOutType],
    IBaseRepository[SchemaInType, SchemaOutType],
):
    """Базовый класс репозитория."""

    model: Type[ModelType]
    schema: Type[SchemaOutType]

    def __init__(self) -> None:
        self.session = db.session

    async def create(self, new_object: SchemaInType) -> SchemaOutType:
        data_model = self.model(**new_object.dict(exclude_unset=True))
        self.session.add(data_model)
        await self.session.flush()
        return await self.get_by_id(data_model.id)

    async def get_by_id(self, object_id: Union[int, UUID]) -> SchemaOutType:
        result = await self.session.execute(select(self.model).where(self.model.id == object_id))
        try:
            obj = result.scalars().one()
        except exc.NoResultFound:
            raise NotFoundException
        return self.serialize_object(obj)

    async def get_all(self) -> list[SchemaOutType]:
        result = await self.session.execute(select(self.model).order_by(self.model.id))
        return self.serialize_list_of_objects(result.scalars().all())

    async def update(
        self, object_id: Union[int, UUID], updated_data: SchemaInType
    ) -> SchemaOutType:
        q = (
            update(self.model)
            .where(self.model.id == object_id)
            .values(**updated_data.dict(exclude_unset=True))
        )
        result: CursorResult = await self.session.execute(q)  # type: ignore
        if result.rowcount != 1:
            raise NotFoundException
        await self.session.flush()
        return await self.get_by_id(object_id)

    async def delete(self, object_id: Union[int, UUID]) -> None:
        result: CursorResult = await self.session.execute(
            delete(self.model).where(self.model.id == object_id)
        )  # type: ignore
        if result.rowcount != 1:
            raise NotFoundException
        await self.session.flush()

    def serialize_object(self, db_data: Base) -> SchemaOutType:
        return self.schema.from_orm(db_data)

    def serialize_list_of_objects(self, db_data: list[Base]) -> list[SchemaOutType]:
        return parse_obj_as(list[self.schema], db_data)  # type: ignore
