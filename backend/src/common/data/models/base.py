import importlib
import re
from typing import Any, Union
from uuid import UUID

from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy.orm import declared_attr


convention: dict[str, Any] = {
    "all_column_names": lambda constraint, table: "_".join(
        [column.name for column in constraint.columns.values()]
    ),
    "ix": "ix__%(table_name)s__%(all_column_names)s",
    "uq": "uq__%(table_name)s__%(all_column_names)s",
    "ck": "ck__%(table_name)s__%(constraint_name)s",
    "fk": "fk__%(table_name)s__%(all_column_names)s__" "%(referred_table_name)s",
    "pk": "pk__%(table_name)s",
}
metadata = MetaData(naming_convention=convention)


@as_declarative(metadata=metadata)
class Base:
    id: Union[int, UUID]
    __name__: str

    __mapper_args__ = {"eager_defaults": True}

    @declared_attr
    def __tablename__(cls) -> str:
        """Автогенерация названия таблицы __tablename__.

        По шаблону {app_name}_{tablename} относительно директории src
        Пример src.exampleapp.models.examplemodel -> exampleapp_examplemodel
        """
        main_dir = "src"
        path = importlib.import_module(cls.__module__).__file__
        pattern = rf"(?<={main_dir}\/)(.*?)(?=\/)"
        try:
            app_name = re.search(pattern, path).group(1)
        except AttributeError:
            raise Exception(
                f"Ошибка автогенерации названия таблицы для {path} "
                f"Нужно разместить таблицу относительно директории {main_dir}/..."
            )
        return f"{app_name}_{cls.__name__.lower()}"
