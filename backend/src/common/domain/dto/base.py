from typing import Optional, Union

from pydantic import UUID4, BaseModel


class OrmSchema(BaseModel):
    class Config:
        orm_mode = True


class BaseSchema(OrmSchema):
    id: Optional[Union[int, UUID4]]


class BaseInSchema(BaseSchema):
    ...


class BaseOutSchema(BaseSchema):
    id: Union[int, UUID4]
