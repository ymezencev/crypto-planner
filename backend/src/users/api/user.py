import uuid

from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends, Response, status

from config.di import Container
from src.users.domain.dto import UserInSchema, UserOutSchema
from src.users.domain.interfaces.user import (
    ICreateUser,
    IDeleteUser,
    IGetUser,
    IListUsers,
    IUpdateUser,
)


router = APIRouter()


@router.get("/", response_model=list[UserOutSchema])
@inject
async def get_users(
    use_case: IListUsers = Depends(
        Provide[Provide[Container.user_package.use_cases].provider.list_users]
    ),
) -> list[UserOutSchema]:
    return await use_case()


@router.get("/{user_id}/", response_model=UserOutSchema)
@inject
async def get_user(
    user_id: uuid.UUID,
    use_case: IGetUser = Depends(
        Provide[Provide[Container.user_package.use_cases].provider.get_user]
    ),
) -> UserOutSchema:
    return await use_case(user_id)


@router.post("/", response_model=UserOutSchema, status_code=status.HTTP_201_CREATED)
@inject
async def create_user(
    new_user: UserInSchema,
    use_case: ICreateUser = Depends(
        Provide[Provide[Container.user_package.use_cases].provider.create_user]
    ),
) -> UserOutSchema:
    return await use_case(new_user)


@router.put("/{user_id}/", response_model=UserOutSchema)
@inject
async def update_user(
    user_id: uuid.UUID,
    user_data: UserInSchema,
    use_case: IUpdateUser = Depends(
        Provide[Provide[Container.user_package.use_cases].provider.update_user]
    ),
) -> UserOutSchema:
    return await use_case(user_id, user_data)


@router.delete("/{user_id}/", status_code=status.HTTP_204_NO_CONTENT)
@inject
async def delete_user(
    user_id: uuid.UUID,
    use_case: IDeleteUser = Depends(
        Provide[Provide[Container.user_package.use_cases].provider.delete_user]
    ),
) -> Response:
    await use_case(user_id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.post("/{user_id}/test", response_model=UserOutSchema)
@inject
async def test_user(
    user_id: uuid.UUID,
    use_case: IGetUser = Depends(
        Provide[Provide[Container.user_package.use_cases].provider.test_user]
    ),
) -> UserOutSchema:
    return await use_case(user_id)
