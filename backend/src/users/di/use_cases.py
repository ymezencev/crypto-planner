from dependency_injector import containers, providers

from src.users.domain.use_cases import CreateUser, DeleteUser, GetUser, ListUsers, UpdateUser
from src.users.domain.use_cases.user import TestUser


class UseCasesContainer(containers.DeclarativeContainer):
    repos = providers.DependenciesContainer()

    list_users = providers.Factory(ListUsers, repo=repos.user)
    get_user = providers.Factory(GetUser, repo=repos.user)
    create_user = providers.Factory(CreateUser, repo=repos.user)
    update_user = providers.Factory(UpdateUser, repo=repos.user, get_user=get_user)
    delete_user = providers.Factory(DeleteUser, repo=repos.user, get_user=get_user)

    test_user = providers.Factory(TestUser, repo=repos.user)
