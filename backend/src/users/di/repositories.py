from dependency_injector import containers, providers

from src.users.data.repositories import UserRepository


class ReposContainer(containers.DeclarativeContainer):
    config = providers.Configuration()
    user = providers.Factory(UserRepository)
