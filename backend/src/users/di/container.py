from dependency_injector import containers, providers

from src.users.di.repositories import ReposContainer
from src.users.di.use_cases import UseCasesContainer


class UserContainer(containers.DeclarativeContainer):
    config = providers.Configuration()
    repos = providers.Container(ReposContainer, config=config)
    use_cases = providers.Container(UseCasesContainer, repos=repos)
