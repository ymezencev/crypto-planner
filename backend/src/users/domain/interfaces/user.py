import abc
import uuid

from src.common.data.repositories import IBaseRepository
from src.users.domain.dto import UserInSchema, UserOutSchema


class IUserRepository(IBaseRepository[UserInSchema, UserOutSchema]):
    ...


class IListUsers(abc.ABC):
    def __init__(self, repo: IUserRepository):
        self.repo = repo

    @abc.abstractmethod
    async def __call__(self) -> list[UserOutSchema]:
        ...


class IGetUser(abc.ABC):
    def __init__(self, repo: IUserRepository):
        self.repo = repo

    @abc.abstractmethod
    async def __call__(self, object_id: uuid.UUID) -> UserOutSchema:
        ...


class ICreateUser(abc.ABC):
    def __init__(self, repo: IUserRepository):
        self.repo = repo

    @abc.abstractmethod
    async def __call__(self, new_object: UserInSchema) -> UserOutSchema:
        ...


class IUpdateUser(abc.ABC):
    def __init__(self, repo: IUserRepository, get_user: IGetUser):
        self.repo = repo
        self.get_user = get_user

    @abc.abstractmethod
    async def __call__(self, object_id: uuid.UUID, updated_object: UserInSchema) -> UserOutSchema:
        ...


class IDeleteUser(abc.ABC):
    def __init__(self, repo: IUserRepository, get_user: IGetUser):
        self.repo = repo
        self.get_user = get_user

    @abc.abstractmethod
    async def __call__(self, object_id: uuid.UUID) -> None:
        ...
