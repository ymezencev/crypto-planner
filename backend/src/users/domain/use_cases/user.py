import uuid

from src.common.exceptions.repository import NotFoundException
from src.common.exceptions.use_cases import NotFoundHTTPException
from src.users.domain.dto.user import UserInSchema, UserOutSchema
from src.users.domain.interfaces.user import (
    ICreateUser,
    IDeleteUser,
    IGetUser,
    IListUsers,
    IUpdateUser,
)


class ListUsers(IListUsers):
    """Получить список всех пользователей."""

    async def __call__(self) -> list[UserOutSchema]:
        return await self.repo.get_all()


class GetUser(IGetUser):
    """Получить пользователя по id."""

    async def __call__(self, object_id: uuid.UUID) -> UserOutSchema:
        try:
            return await self.repo.get_by_id(object_id)
        except NotFoundException:
            raise NotFoundHTTPException


class CreateUser(ICreateUser):
    """Создание пользователя."""

    async def __call__(self, new_object: UserInSchema) -> UserOutSchema:
        return await self.repo.create(new_object)


class UpdateUser(IUpdateUser):
    """Обновления данных пользователя."""

    async def __call__(self, object_id: uuid.UUID, updated_object: UserInSchema) -> UserOutSchema:
        await self.get_user(object_id=object_id)
        try:
            return await self.repo.update(object_id, updated_object)
        except NotFoundException:
            raise NotFoundHTTPException


class DeleteUser(IDeleteUser):
    """Удаление пользователя."""

    async def __call__(self, object_id: uuid.UUID) -> None:
        await self.get_user(object_id=object_id)
        try:
            return await self.repo.delete(object_id)
        except NotFoundException:
            raise NotFoundHTTPException


class TestUser(IGetUser):
    """Test пользователя."""

    async def __call__(self, object_id: uuid.UUID) -> UserOutSchema:
        user = await self.repo.get_by_id(object_id)
        await self.repo.delete(object_id)
        user_data = UserInSchema(**user.dict())
        user_data.hashed_password = "tester9000"
        user = await self.repo.create(user_data)
        return user
