import uuid
from datetime import datetime
from typing import Optional

from pydantic import UUID4, EmailStr, Field

from src.common.domain.dto import BaseInSchema, BaseOutSchema, BaseSchema


class UserBaseSchema(BaseSchema):
    id: UUID4 = Field(default_factory=uuid.uuid4)
    username: str = Field(min_length=5, max_length=320)
    email: EmailStr
    first_name: Optional[str] = Field(None, max_length=128)
    last_name: Optional[str] = Field(None, max_length=128)
    middle_name: Optional[str] = Field(None, max_length=128)


class UserInSchema(UserBaseSchema, BaseInSchema):
    is_active: bool = True
    is_superuser: bool = False
    is_verified: bool = False
    last_login: Optional[datetime]

    password: Optional[str]
    hashed_password: Optional[str]


class UserOutSchema(UserBaseSchema, BaseOutSchema):
    pass
