import uuid

from sqlalchemy import Boolean, Column, DateTime, String, func
from sqlalchemy.dialects.postgresql import UUID

from src.common.data.models import Base, TimestampMixin


class User(Base, TimestampMixin):
    __tablename_args__ = {"comment": "Пользователь"}

    id: UUID = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    username = Column(String(length=15), unique=True, index=True, nullable=False)
    email = Column(String(length=320), unique=True, index=True, nullable=False)
    first_name = Column(String(128), nullable=True)
    last_name = Column(String(128), nullable=True)
    middle_name = Column(String(128), nullable=True)

    hashed_password = Column(String(length=72), nullable=False)
    is_active = Column(Boolean, default=True, nullable=False)
    is_superuser = Column(Boolean, default=False, nullable=False)
    is_verified = Column(Boolean, default=False, nullable=False)
    last_login = Column(DateTime, default=func.now())

    def __repr__(self) -> str:
        return f"Пользователь: {self.username}"

    @property
    def fio(self) -> str:
        return f"{self.last_name} {self.first_name} {self.middle_name}"
