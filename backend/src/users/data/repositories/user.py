from src.common.data.repositories import BaseRepository
from src.users.data.models import User
from src.users.domain.dto import UserInSchema, UserOutSchema
from src.users.domain.interfaces.user import IUserRepository


class UserRepository(IUserRepository, BaseRepository[User, UserInSchema, UserOutSchema]):
    model = User
    schema = UserOutSchema
