import uvicorn

from config.di import Container, DependencyInjectorFastApi
from config.middleware import initialize_middleware
from config.routers import initialize_routers


def initialize_app() -> DependencyInjectorFastApi:
    container = Container()
    application = DependencyInjectorFastApi(
        title=container.config.app.name(),
        debug=container.config.app.debug(),
    )
    application.container = container
    initialize_middleware(application)
    initialize_routers(application)
    return application


app = initialize_app()


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=app.container.config.app.server_host(),
        port=app.container.config.app.server_port(),
        reload=True,
    )
