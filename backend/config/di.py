from dependency_injector import containers, providers
from fastapi import FastAPI

from src.users.di.container import UserContainer

from .settings import settings


class Container(containers.DeclarativeContainer):
    config = providers.Configuration(pydantic_settings=[settings])
    wiring_config = containers.WiringConfiguration(packages=settings.app.wiring_modules)

    user_package = providers.Container(UserContainer, config=config)


class DependencyInjectorFastApi(FastAPI):
    container: Container
