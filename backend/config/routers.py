from fastapi import APIRouter

from config.di import DependencyInjectorFastApi
from src.users.api import user_router


router = APIRouter()
router.include_router(user_router, prefix="/users", tags=["users"])


def initialize_routers(application: DependencyInjectorFastApi) -> None:
    """Инициализация routers в приложении."""
    application.include_router(router, prefix="/api")
