from typing import Any, Optional, Sequence

from pydantic import AnyUrl, BaseSettings, Field, validator


class EnvBaseSettings(BaseSettings):
    class Config:
        env_file = "../.env"
        env_file_encoding = "utf-8"


class AppSettings(EnvBaseSettings):
    name: str = "Crypto Planner"
    secret_key: str = Field("12345678", env="SECRET_KEY")
    environment: str = Field("production", env="ENVIRONMENT")
    debug: bool = Field(False, env="DEBUG")
    server_host: str = Field("localhost", env="SERVER_HOST")
    server_port: int = Field(8000, env="SERVER_PORT")
    cors_origins: Sequence[str] | str = "*"

    apps: list[str] = [
        "users",
    ]
    wiring_modules: list[str] = []

    @validator("wiring_modules", pre=True)
    def assemble_wiring_modules(cls, v: list[str], values: dict[str, Any]) -> list[str]:
        for app in values["apps"]:
            v.append(f"src.{app}.api")
        return v


class DatabaseSettings(EnvBaseSettings):
    class PostgresDsn(AnyUrl):
        allowed_schemes = {"postgresql+asyncpg"}
        user_required = True

    scheme: str = "postgresql+asyncpg"
    host: str = Field("localhost", env="POSTGRES_HOST")
    port: str = Field("5432", env="POSTGRES_PORT")
    user: str = Field("postgres", env="POSTGRES_USER")
    password: str = Field("postgres", env="POSTGRES_PASSWORD")
    db: str = Field("postgres", env="POSTGRES_DATABASE")
    autoflush: bool = False
    autocommit: bool = False
    expire_on_commit: bool = False
    dsn: Optional[PostgresDsn] = None

    @validator("dsn", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: dict[str, Any]) -> str:
        if isinstance(v, str):
            return v
        return DatabaseSettings.PostgresDsn.build(
            scheme=values.get("scheme"),
            user=values.get("user"),
            password=values.get("password"),
            host=values.get("host"),
            port=values.get("port"),
            path=f"/{values.get('db')}",
        )


class Settings(EnvBaseSettings):
    app: AppSettings = AppSettings()
    database: DatabaseSettings = DatabaseSettings()


settings = Settings()
