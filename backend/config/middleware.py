from debug_toolbar.middleware import DebugToolbarMiddleware
from fastapi_async_sqlalchemy import SQLAlchemyMiddleware

from config.di import DependencyInjectorFastApi
from src.common.data.database.base import engine


def initialize_middleware(application: DependencyInjectorFastApi) -> None:
    """Инициализация middleware в приложении."""
    if application.container.config.app.debug:
        application.add_middleware(
            DebugToolbarMiddleware,
            panels=["debug_toolbar.panels.sqlalchemy.SQLAlchemyPanel"],
            SHOW_COLLAPSE=True,
            SQL_WARNING_THRESHOLD=100,
        )

    application.add_middleware(SQLAlchemyMiddleware, custom_engine=engine, commit_on_exit=True)
